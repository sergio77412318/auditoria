// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyA5JSjnOL3uvzfPIYqJSveeZ2UkxpIYpF4",
    authDomain: "auditoria-26cea.firebaseapp.com",
    databaseURL: "https://auditoria-26cea.firebaseio.com",
    projectId: "auditoria-26cea",
    storageBucket: "auditoria-26cea.appspot.com",
    messagingSenderId: "978069876860",
    appId: "1:978069876860:web:7cfa090465034f6f4899f9",
    measurementId: "G-8YSJ11QPJ2" 
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
